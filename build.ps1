$AWS_REGION="ap-southeast-1"
$AWS_USERNAME="AWS"
$AWS_ECR_URL="912611140120.dkr.ecr.ap-southeast-1.amazonaws.com"


Write-Output "Building Simple-chat app.."

aws ecr get-login-password --region $AWS_REGION | docker login --username $AWS_USERNAME --password-stdin $AWS_ECR_URL


docker build -t simple-chat .

Write-Output "Build completed!"

Write-Output "Start pushing to ECR"

docker tag simple-chat:latest $AWS_ECR_URL/simple-chat:latest

docker push $AWS_ECR_URL/simple-chat:latest

Write-Output "Pushed image to ECR!"