from threading import local
from flask import Flask, render_template, request
import pytz
import pymysql
# import os
import datetime

app = Flask(__name__)

# connect mysql
connection = pymysql.connect(host='simple-chat.cga1cl3fpzg2.ap-southeast-1.rds.amazonaws.com',
                             user='admin',
                             password='Aa7404275',
                             db='simplechat',
                             port=3306)

# host = os.uname()[1]


def convertTuple(tup):
    # initialize an empty string
    string_converted = ''
    for item in tup:
        string_converted = string_converted + str(item) + " "
    return string_converted


@app.route("/")
def home():
    return render_template('index.html')


@app.route("/<room>")
def chat(room):
    return render_template('index.html')


@app.route("/chat/<room>")
def chat_1(room):
    return render_template('index.html')


@app.route("/api/chat/<room>", methods=["GET", "POST"])
def chat_room(room):
    lines = None
    if request.method == "POST":
        tz_hcm = pytz.timezone("Asia/Ho_Chi_Minh")
        start_utc = datetime.datetime.now(tz_hcm)
        time_now = start_utc.strftime("%Y-%m-%d %X")
        username = request.form["username"]
        mess = request.form["msg"]

        # Creating a connection cursor
        cursor = connection.cursor()

        # cursor.execute('''USE NodeDB''')
        # Executing SQL Statements
        cursor.execute(
            '''CREATE TABLE IF NOT EXISTS chat(room varchar (15), times varchar(50), username varchar (50), mess varchar(50))''')
        cursor.execute(''' INSERT INTO chat(room,times,username,mess) VALUES(%s,%s,%s,%s)''',
                       (room, time_now, username, mess))
        # Saving the Actions performed on the DB
        cursor.connection.commit()
        # Closing the cursor
        cursor.close()
        return ''

    else:
        cursor = connection.cursor()
        cursor.execute(''' SELECT * FROM chat WHERE room = %s''', (room,))
        dbs = cursor.fetchall()

        str_result = 'Load balancing server: ' + "\n\n"
        for line in dbs:
            str_result = str_result + convertTuple(line) + '\n'
        cursor.close()
        return str_result


if (__name__ == '__main__'):
    app.run('0.0.0.0', port=5000, debug=True)
